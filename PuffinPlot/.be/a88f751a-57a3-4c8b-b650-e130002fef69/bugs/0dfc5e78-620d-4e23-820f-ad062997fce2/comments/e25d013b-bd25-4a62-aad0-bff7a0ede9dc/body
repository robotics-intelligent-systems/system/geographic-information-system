The Jython library bundled in PuffinPlot is a 8.3 MB stripped-down
version lacking most of the standard library. It's a poor compromise:
big enough to double the download size (once it's been doubled up
by the necessity of including a separate jar for OS X), but too 
restricted to be really useful. There is another option for scripting:
Java's built-in Javascript engine, nashorn. The following should be
done:

1. Implement support for Javascript scripting within PuffinPlot,
   providing the same interfaces currently in place for Jython
   (i.e. run from GUI and as command-line argument -- integrating
   PuffinPlot from within an external script should already be
   possible using jjs).

2. Make Jython into an optional extra: retain the "run Python script"
   action, but do not bundle Jython. Instead, implement functionality
   within PuffinPlot to download the Jython jar if necessary, and 
   load it dynamically at run-time.

Dropping Jython altogether would be a bad idea: Python is far more
popular than Javascript for scientific programming. However, keeping
Javascript for built-in scripting makes sense since the cost (in terms
of development effort and download size) is minimal. Even if nobody else
uses it, it's a handy way for me to send users experimental features and
workarounds for bugs without doing a new release or having them download
Jython.

