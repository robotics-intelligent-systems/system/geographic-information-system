Further details following discussion with Christian:

What we are looking to calculate is known as the `median destructive
field' (or, presumably, temperature, though this seems less common),
which is the field required to reduce the intensity to one half of its
initial value. It's described in Dunlop and Özdemir (p. 230). So all
we need to do is determine where the intensity curve crosses the
horizontal line corresponding to half the initial intensity, and read
off the corresponding field strength (or temperature). Interpolating
between two demagnetization levels will probably be necessary.

It's a little more complicated than that, though. Firstly, think about
NRMs with multiple components in different directions. The absolute
intensity might go up before it goes down, as the softest component
disappears first. What do we do then? There is no obvious right
answer. The way to implement it is probably to use the initial
intensity by default, but allow the user to manually recalculate using
only selected points.

Secondly, what if there's more than one crossing? Probably easy: pick
the first (likely to be the one we care about), and again have manual
restricted recalculation as a fall-back.

Thirdly, what if there's no crossing (i.e. we never get down to half
intensity)? Just go with the maximum field/temperature, says
Christian: it will stand out anyway when graphed. But perhaps it
should be marked in some other way when saving the values.

MDF should be marked on the graph by orthogonal lines projecting from
the axes (as is Tauxe lecture 9) and saveable as CSV (can presumably
be thrown in with PCA and Fisher values).
